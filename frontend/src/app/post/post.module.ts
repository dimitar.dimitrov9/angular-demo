import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostRoutingModule } from './post-routing.module';
import { AllPostsComponent } from './all-posts/all-posts.component';
import { PostComponent } from './post/post.component';
import { SharedModule } from '../shared/shared.module';
import { PostCardComponent } from './post-card/post-card.component';



@NgModule({
  declarations: [
    AllPostsComponent,
    PostComponent,
    PostCardComponent
  ],
  imports: [
    CommonModule,
    PostRoutingModule,
    SharedModule
  ]
})
export class PostModule { }
