import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../post'

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent implements OnInit {
  @Input('post') post: Post | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
