import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appCursorPointer]'
})
export class CursorPointerDirective {

  constructor(private el: ElementRef) {
    this.el.nativeElement.style.cursor = 'pointer';
  }

}
